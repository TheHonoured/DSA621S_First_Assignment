import  ballerina/http;

listener  http:Listener  ep0  = new (9090, config  = {host: "localhost"});

 service on ep0 {
        resource function get learner_profile(LearnerProfile[] learner_profiles) returns ArrayOfLearnerProfiles|record  {|*http:NotFound; string  body;|}|record  {|*http:Ok; string  body;|} {
            ArrayOfLearnerProfiles array_of_learner_profiles = { learner_profiles: learner_profiles};
            return array_of_learner_profiles;
        }

        isolated resource  function  post  learner_profile(@http:Payload LearnerProfile learner_profile, LearnerProfile[] learner_profiles)  returns  string|record  {|*http:NotFound; string  body;|}|record  {|*http:Ok; string  body;|} {
            learner_profiles.push(learner_profile);
            return "Learner Profile been added successfuly";
        }

        resource function get learner_profile/[string  user_name](LearnerProfile[] learner_profiles) returns LearnerProfile|record {|*http:NotFound; string  body;|}|record  {|*http:Ok; string  body;|} {
            LearnerProfile learner;
            LearnerProfile[] learners = [];
            
            learners  = 
            from var learner_profile in learner_profiles
            let string? u = learner_profile?.user_name
            where u == user_name
            select learner_profile;
            
            learner = learners[0];

            return learner;        
        }

        resource  function  put  learner_profile/[string  user_name]()  returns  string|record  {|*http:NotFound; string  body;|}|record  {|*http:Ok; string  body;|} {
            boolean updated = doUpdate(user_name);
            if updated {
                return user_name;
            }
            return "Update failed please try again";
        }
}

function doAppend( LearnerProfile l_profile) {

}

function doUpdate(string u_name) returns boolean {
    int count = 0;
    LearnerProfile? learner_profile = ();
    foreach var l_profile in learner_profiles {
        if l_profile?.user_name == u_name {
            learner_profile = l_profile;
            break;
        }
        count += 1;
    }
    if learner_profile != () {
        learner_profiles.push(learner_profile);
        var removed_learner = learner_profiles.remove(count);
        return true;
    }
    return false;
}