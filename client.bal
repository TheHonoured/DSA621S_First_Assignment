import  ballerina/http;

# This is a an api that retrieves, creates and updates learner profiles, whilst genereting custom course material of the learner
#
# + clientEp - Connector http endpoint
@display {label: "KRUSH API"}
public client class Client {
    http:Client clientEp;
    public isolated function init(http:ClientConfiguration clientConfig =  {}, string serviceUrl = "http://localhost:9090") returns error? {
        http:Client httpEp = check new (serviceUrl, clientConfig);
        self.clientEp = httpEp;
    }
    # Gets all Learner Profiles
    #
    # + return - A list of Learner Profiles
    remote isolated function getAllLearnerProfiles() returns ArrayOfLearnerProfiles|error {
        string  path = string `/learner_profile`;
        ArrayOfLearnerProfiles response = check self.clientEp-> get(path, targetType = ArrayOfLearnerProfiles);
        return response;
    }
    # Creates a Learner Profile
    #
    # + return - Successful operation
    remote isolated function createLearnerProfile() returns string|error {
        string  path = string `/learner_profile`;
        http:Request request = new;
        //TODO: Update the request as needed;
        string response = check self.clientEp-> post(path, request, targetType = string);
        return response;
    }
    # Gets Learner Profile by user name
    #
    # + user_name - Learner_Profile user_name
    # + return - Successful operation
    remote isolated function getLearnerProfile(string user_name) returns LearnerProfile|error {
        string  path = string `/learner_profile/${user_name}`;
        LearnerProfile response = check self.clientEp-> get(path, targetType = LearnerProfile);
        return response;
    }
    # Updates Learner Profile by user name
    #
    # + user_name - Learner_Profile user_name
    # + return - Successful operation
    remote isolated function updateLearnerProfile(string user_name) returns string|error {
        string  path = string `/learner_profile/${user_name}`;
        http:Request request = new;
        //TODO: Update the request as needed;
        string response = check self.clientEp-> put(path, request, targetType = string);
        return response;
    }
}
