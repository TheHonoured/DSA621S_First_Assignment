public LearnerProfile[] learner_profiles = [
{
    user_name: "moonvader101",
    first_name: "Kuizikee",
    last_name: "Maendo",
    preferred_formats: ["video", "text"],
    courses: courses
},
{
    user_name: "hoosainshakeel",
    first_name: "Shakeel",
    last_name: "Hoosain",
    preferred_formats: ["video", "audio"],
    courses: courses
}
];
public Course[] courses = [];