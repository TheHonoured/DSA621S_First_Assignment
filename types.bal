public type ArrayOfLearnerProfiles record {
    LearnerProfile[] learner_profiles;
};

public type LearnerProfile record {|
    # to be used as the identifier usernames must be unique
    readonly string user_name;
    # last name of the learner
    string last_name?;
    # first name of the learner
    string first_name?;
    # the formats that the learner prefers to be taught in
    string[] preferred_formats?;
    # courses available to the student
    Course[] courses?;
|};

public type Items record {
    # the type of format for the course material
    string item_format?;
    # the name/topic of the certain material
    string name?;
    # the description of this learning item
    string item_description?;
    # the difficulty of this item
    string difficulty?;
};

public type Course record {
    # the name of the subject/course
    string name?;
    # the grades that the learner acquired in this subject
    string grade?;
    # used to check if the subject has been completed successfuly or not
    int status?;
    CourseMaterial course_material?;
};

public type CourseMaterial record {
    Items[] required_materials?;
    # the material the student does not necessarily have to learn from
    Items[] suggested_materials?;
};
